---
title: "Dawid Cygert"
date: 2018-11-19T10:47:58+10:00
draft: false
image: "images/team/Dawid Cygert.jpeg"
jobtitle: "Bachelor of Engineering"
# linkedinurl: "https://www.linkedin.com/"
promoted: true
weight: 3
---

Technology expert, Software engineer
