# website
A simple website built with hugo.

## Getting Started
To get a local copy up an running follow these simple steps.

1. Clone the repository with submodules
```
git clone --recurse-submodules https://gitlab.com/airq/website.git 
```
2. Run the hugo server locally
```
hugo server -p <port>
```

## Prerequisites
Install the requirements.
- [hugo](https://github.com/gohugoio/hugo) v0.101.0+ (extended)

## FAQ
### Git submodules not showing up
```
git submodule init
git submodule update
```