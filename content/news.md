---
title: 'News'
date: 2018-02-22T17:01:34+07:00
---
#### 2023.11.07 - IBM TechConnect Warsaw 2023
![1 Services](../images/news_07.11.23_.jpg)

On November 7, the [IBM conference](https://www.ibm.com/events/reg/flow/ibm/v7zvvfmb/landing/page/landing) was held in the PGE Narodowy halls in Warsaw devoted to the prospects for the development of generative artificial intelligence systems. At the conference, we talked not only about IBM technologies such as [Watsonx](https://www.ibm.com/watsonx) to support the production processes of such systems, but also about the experiences of business and universities related to the design and use of such systems.

During the discussion panel, I talked about projects implemented at our University related to supporting the teaching process using such systems, but also about teaching programs under which our students gain full knowledge of the automation of production processes based on containerization and service orchestration, as well as infrastructure management processes using infrastructure as code approach. This approach to the curricula created conditions for the use of student certification processes and thus for preparing them to produce such complex systems as generative artificial intelligence systems in production environments such as Watsonx proposed by IBM.

#### 2023.10.01 - A significant stage in the development of IBM CAS WSB MERITO Team
October 1, 2023 - Mr. Piotr Pietrzak IBM Executive Architect was invited to the inauguration of the academic year. During a short speech, he talked about the next stages of development of IBM CAS WSB, pointing to the key project subject to commercialization, which is the development of a system to support Clinical Emergency Departments.

#### 2023.07.24 - 230 certificates were issued
In the summer semester 2023, IBM CAS WSB coordinated the process of issuing 230 certificates:
- Getting Started with Enterprise-grade AI.
- Getting Started with Enterprise Data Science.
- Journey to Cloud: Envisioning Your Solution.

#### 2023.06.25 - IBM CAS WSB becomes IBM's business partner
On June 25, 2023, another agreement was signed under which IBM CAS WSB becomes IBM's business partner. This is another stage of the center's development focused on the implementation of business projects.

#### 2023.02.28 - New development agreement for IBM CAS WSB
On February 28, 2023, a new long-term development agreement for IBM CAS WSB was signed. This agreement significantly extends the scope of existing activities of IBM CAS. New activities of IBM CAS will concern research projects with business partners as well as didactics and certification of students.

#### 2022.11.08 Think Warsaw 
On November 8, Think Warsaw was held in Warsaw
https://www.ibm.com/events/think/on-tour/warsaw/
This is one of those events you should be attending. Organized by a large IT company and promoting the most important IT trends. I was pleased to see how IBM implements the strategy of using AI, Hybrid Cloud, cybersecurity, and data driven development. But I also felt how this strategy translates into a change in the way our IT students are educated at WSB.

#### 2022.10.08-2022.10.15
On October 8, 15, Professor Cezary Orłowski stayed in IBM Dubai. The purpose of the meetings was to prepare Erasmus trips for our students to IBM research centers around the world. During the meetings, there was also talk about the possibility of wanting students at HCT in Dubai, currently an offer of cooperation is being prepared.
#### 2022.06.04- IBM CAS as a DEvOps team
We made a decision to separate two teams in the IBM CAS team: development and operational for the implementation of CAS objectives and projects. This decision resulted from problems with the implementation of too many projects and difficulties on the implementation side, as well as on the delivery of the system to our clients. We assumed that the first new project will contain information about the possibility of building containers and their implementation.

#### 2022.05.02- IBM CAS Gdańsk finishes the IoT air pollution project
On 29th November the JazzOps team handed over 3 air quality sensors to residents of the Gdańsk Strzyża district. The project was supported by Airly and SoftCad companies aimed to provide residents with the technology that helps to measure, predict and prevent the increase in air pollution in key areas of the district.

 

During two years of cooperation with the FRAG institution and mentioned partners JazzOps team created 3 sensors based on the Internet of Things technology and AI algorithms. Sensors measure the level of PM10 particles and analyze data provided by GIOŚ and Luftdaten measurement stations. Together with sensor modules, the JazzOps team created an application that calculates prognosis of the air pollution and a user-friendly map that helps to verify air quality in given spots and take preventive actions if needed.

 

The project team consisted of two developers: Aleksander Kleszczewski and Adam Rumiński under mentorship of project’s leader - Maciej Kacperski.

#### 2021-10-01 - IBM CAS Gdańsk opens IoT Lab at the WSB University in Gdańsk
IBM in Poland strengthens cooperation with Polish universities - WSB University in Gdańsk and Silesian University of Technology.

Higher School of Banking in Gdańsk opens an Internet of Things Technology Research Laboratory (IBM CAS Lab IoT) with the purpose of developing architecture of IoT Edge-Fog-Cloud and Big Data processing with the use of Artificial Intelligence methods. 

“IBM and Higher School of Banking can be proud of a long cooperation history. In 2015 we launched the Centre of Advanced Studies (IBM CAS). Now, we give to students and scientists a new space - Internet of Things Laboratory on newly established Computer Science and New Technologies faculty, that will let us widen research on IoT nodes with support of advanced IBM technology” - says prof. dr. hab. inż Cezary Orłowski, Head of CAS IBM at Higher School of Banking in Gdańsk.
