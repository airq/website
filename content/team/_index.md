---
title: 'Team'
# intro_image: "images/team/12.jpg"
intro_image_absolute: false
intro_image_hide_on_mobile: false
---

# JazzOps Team

Our team members integrate the experience of developing and delivering IT systems
