---
title: 'About'
date: 2018-02-22T17:01:34+07:00
---

## Establishment of Centre for Advanced Studies at MERITO WSB University
The finalizing meeting of Centre for Advanced Studies at Gdansk University of Technology (CAS at WSB) establishment took place on 27th April 2016 at WSB University. CAS is a research and development unit focused on the implementation of projects in the field of information technology in relation to the global business strategy of IBM, which is a partner of the centre.
![1 Services](../images/poczatek.jpg)

**Figure 1.** Establishment of CAS


## CAS at WSB

Centre for Advanced Studies at WSB University in Gdansk (CAS at WSB) is a research and development unit established on the university campus to focus on the implementation of projects in the field of information technology in relation to the global business strategy of IBM – a partner of the centre. CAS establishment within WSB University structures offers opportunities for multidisciplinary cooperation of staff, students and doctoral students from different WSB faculties with highly skilled professionals from cooperating with CAS entities. Such an approach is consistent with the requirements of today's knowledge-based economy, in which interdisciplinary teams play a major role in R&D projects conducting. In this context, it becomes crucial to build an open and favorable environment of mutual relations between science and business organizations.


## The vision of the Centre 



The vision of the Centre is to become a leading research and development unit of north-eastern Poland in the field of IBM Information Technology Services. 

  

CAS at WSB, according to its mission, undertakes to build, maintain and develop a favourable environment for exchange of experience between academic staff and business representatives to enable effective and efficient R&D projects implementation in the field of new technologies, especially IT and decision support systems. Therefore the main goals of CAS at WSB are: 

- developing mutual relations between science and business organizations, 
- acquiring the status of outstanding R&D unit recognizable domestically and abroad, 
- cooperation with business organizations, 
- training and development opportunities for the University staff and students, 
- promoting and developing of project management standards. 

According to the CAS mission as a place of personal development of employees and PhD, MSc and BSc students, the founders would like to invite all interested representatives of the academic community to the cooperation. 


## Research Fields

CAS Executive Advisory Board (in cooperation with IBM) has fixed four lines of research: 

- IoT and mobile technology (IoT) 
- Artificial Intelligence 
- Processing technology of significant resources (Big Data) 
- Hybrid Cloud. 

## **Members of IBM Centre for Advanced Studies**

### Prof. Cezary Orłowski 
Head of IBM Centre for Advanced Studies Institute of Management and Finance. Specialist in the application of IoT systems, Big Data pre-processing methods especially fuzzy and neuro modelling, building Infrastructure as a Code (IaaC) and digital transformation in environmental engineering. 

### **IBM CAS WSB MERITO Team**
### Artur Ziółkowski
Academic teacher
 ### Łukasz Kryła
Technology specialist
### Marek Ożarowski
Chat specialist
### Adam Doering
Industry 4.0 specialist
### Łukasz Muszarski
Student’s projects specialist
### Sebastian Agata
Cybersecurity specialist

### **JazzOps Team**
### Mariusz Wąsik
Technology, Infrastructure expert
### Dawid Cygert
Technology expert, Software engineer
### Maciej Trawka
Technology expert, IoT advisor
### Igor Bublij
Technology, Ui/Ux specialist
### Marcin Andrych
Technology specialist, Software, Node-Red engineer
### Mateusz Orzechowski
Technology specialist, Mobile application developer
### Maciej Kamiński
Technology specialist

