---
title: 'IoT lab in IBM CAS'
date: 2018-02-22T17:01:34+07:00
---
## Goal of building this lab

On October 1, the Internet of Things Technology Research Laboratory (IBM CAS Lab IoT) was opened at the WSB University in Gdańsk. The purpose of this laboratory is the development of IoT Edge-Fog-Cloud system architectures and Big Data processing with the use of artificial intelligence methods. The establishment of this laboratory is the result of over 10 years of cooperation between Gdańsk's academic centers and IBM. The establishment of the IBM CAS Advanced Research Center at the Gdańsk University of Technology in 2006 and 2015, the IBM CAS Advanced Research Center at the WSB University in Gdańsk, is the next stage in the development of cooperation between one of the largest IT companies and research and development units in Gdańsk.

![1 Services](../images/arch1.png)

**Figure 1.** The local environment and the development environment in the IoT laboratory

The establishment of this laboratory will significantly contribute to the development of computer science at the WSB University in Gdańsk at the newly established Faculty of Computer Science and New Technologies. It will also contribute to the development of research on IoT nodes and the use of advanced IBM technologies for researching these nodes, says Professor Cezary Orłowski from the WSB University in Gdańsk.

The establishment of this laboratory strengthened the status of the IBM CAS existing at the WSB University in Gdańsk and will allow the Center to be registered on the list of global IBM IoT research centers. It also enables cooperation with many such IBM centers that carry out research tasks not only in the field of IoT, but also in the use of artificial intelligence methods and Big Data processing - the main development trends of IBM and modern IT, says Mariusz Ochla, director of the technical cloud education department of the IBM MEA Skills Academy program.

![1 Services](../images/Iot1.jpg)

**Figure 2.** Three Rapberry Pi clusters (five physical machines for every cluster) on the wall



