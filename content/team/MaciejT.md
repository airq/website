---
title: "Maciej Trawka"
date: 2018-11-19T10:47:58+10:00
draft: false
image: "images/team/Maciej_T.jpg"
jobtitle: "Doctor of engineering"
# linkedinurl: "https://www.linkedin.com/"
promoted: true
weight: 4
---
Technology expert, IoT advisor