---
title: 'CAS Executive Advisory Board'
date: 2018-02-22T17:01:34+07:00
---
## Goals of EAB


The function of the IBM Center for Advanced Studies Executive Advisory Board at WSB University of Technology (CAS at WSB) is to set the direction of information technology research run by IBM CAS Gdansk. Another CAS Committee’s goal is monitoring and documenting the progress of research in both IBM publications and scientific journals (inter alia 3372 IBM JOURNAL OF RESEARCH AND DEVELOPMENT and 3373 IBM SYSTEMS JOURNAL) in following fields (according to the Web of Science):
- Computer Science Theory, Methods 
- Computer Science Information Systems 
- Computer Science Artificial Intelligence 
- Computer Science Cybernetics 

To achieve these goals CAS Executive Advisory Board will collaborate with IBM research laboratories dealing with issues similar to the research conducted by CAS Gdansk. It is planned to establish a cooperation with the laboratories of IBM in Zurich and Dublin.
CAS Executive Advisory Board (in cooperation with IBM) has fixed four lines of research for 2014-2020:
- IoT and mobile technology (IoT) 
- processing technology of significant resources (Big Data) 
- software development environments (Software Defined Environments) 
- processing technologies in the cloud computing (Cloud Computing) 
It was assumed that the Executive Advisory Board will work with the use of remote communication systems, and will review work of CAS Gdansk quarterly.

# Members
## Director CAS Prof. dr hab. inż. Cezary Orłowski
![1 Services](../images/cezary.jpg)

phone:          +48 508 87 67 84
e-mail:    Cezary.Orlowski@wsb.gda.pl
address:     313 B Building WSB University in Gdańsk, Aleja Grunwaldzka 238 A, 80-266 Gdansk

## prof. dr hab. inż. Edward Szczerbicki
![1 Services](../images/edek.jpg)

e-mail:    Edward.Szczerbicki@zie.pg.gda.pl
room:     Gdansk University of Technology, 820 B Building


## Prof. Kostas Karatzas 

![1 Services](../images/kostas.png)

Professor of Environmental Informatics and Informatics Systems at the Department of Mechanical Engineering, Aristotle University of Thessaloniki, Greece. He works mainly in the field of environmental informatics, data analytics and modelling for environmental and engineering applications, urban environment management and information systems, environmental forecasting with the aid of data mining - computational intelligence methods and mathematical models, quality of life - environment related - information services and computational improvement of environmental microsensors performance. 

## Prof. Manuel Graña, PhD. Eng.
![1 Services](../images/manuel.jpg)

Address: Facultad de Informática de San Sebastián
Phone numbers: 943 01 8044 (Prof. Graña) - 943 01 5106 (Lab. 308) - 943 01 8047 (Lab. 307)
Fax: 943 01 5590


## Mariusz Ochla, TEC Manager, Head of CAS Poland, IBM Polska
![1 Services](../images/mariuszO.jpg)

tel: (22) 878 6622
email: mariusz_ochla@pl.ibm.com
IBM Polska, ul. Krakowiaków 32, 02-255 Warszawa


## Piotr Pietrzak, Chief Technology Officer, IBM Polska Member of IBM Academy of Technology
![1 Services](../images/piotr.jpg)

tel: (22) 878 6621
email: piotr.pietrzak@pl.ibm.com
IBM Polska, ul. Krakowiaków 32, 02-255 Warszawa


## Prof. Ngoc-Thanh Nguyen, PhD. Eng.
![1 Services](../images/than.jpg)

phone:       +48 71 320-41-39
e-mail:   Ngoc-Thanh.Nguyen@pwr.wroc.pl
The Institute of Informatics in the Faculty of Computer Science and Management


## Prof. Zdzisław Kowalczuk, PhD. Eng.
![1 Services](../images/zdzichu.jpg)

phone:    +48 (58) 347 20 18
e-mail:    kova@eti.pg.gda.pl
The Faculty of Electronics, Telecommunications and Informatics

## Dr hab. inż. Piotr Cofta prof. Uczelni
![1 Services](../images/PCofta.jpg)

Prof. Piotr Cofta received his Ph.D. from the Gdansk University of Technology, Poland and his D.Sc.
from the Polish-Japanese Academy of Information Technology. He is currently is with the WSB
University Gdańsk as a professor of informatics. He combines his international experience, strict
methodological approach, and extensive practical knowledge. His research interest focuses on trust
and uncertainty, with applications to IoT sensor networks and swarms.

## Johanna Paul Henao Garcia
![1 Services](../images/johanna_paula_henao_garcia.jpg)

Coordinator of scientific and academic contacts in Spanish-speaking countries (Spain and Latin America).

