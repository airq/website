---
title: 'Homepage'
meta_title: 'IBM CAS WSB Gdańsk'
description: "Serif is a modern business theme for Hugo."
# intro_image: "images/illustrations/pointing.svg"
intro_image: "images/illustrations/studenci.jpg"
intro_image_absolute: true
intro_image_hide_on_mobile: true
---

# IBM CAS WSB GDAŃSK

CAS is a research and development unit focused on the implementation of projects in the field of information technology in line with global business strategy of IBM, which is a partner of the centre. The finalizing meeting of Centre for Advanced Studies (CAS at WSB) establishment took place on 27th April 2016 at WSB University.
