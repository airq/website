---
title: 'Services'
description: 'We offer a range of services to help your business'
# intro_image: "images/illustrations/reading.svg"
# intro_image: "images/illustrations/studenci.jpg"
intro_image: "images/illustrations/arch2.png"
intro_image_absolute: true
intro_image_hide_on_mobile: false
---

# IBM CAS projects

This is a list of projects implemented by IBM CAS in Gdansk. These projects were implemented in 2016-2022 and were divided into four groups. Group a project is in progress. Group b projects have been completed or are in the termination stage. In turn, the projects of groups c and d have been completed.
